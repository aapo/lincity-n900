/*
Copyright (C) 2005 Matthias Braun <matze@braunis.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifdef GLES
#include <config.h>

#include "PainterGLES.hpp"

#include <SDL.h>
#include <GLES/gl.h>
#include <SDL_gles.h>
#include <iostream>
#include <typeinfo>

#include "TextureGLES.hpp"

PainterGLES::PainterGLES()
{
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

PainterGLES::~PainterGLES()
{
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}

void
PainterGLES::drawTextureRect(const Texture* texture, const Rect2D& rect)
{
    const TextureGLES* textureGL = static_cast<const TextureGLES*> (texture);
    const Rect2D& r = textureGL->rect;

    glColor4ub( 0xff, 0xff, 0xff, 0xff );

    glBindTexture(GL_TEXTURE_2D, textureGL->handle);

    GLfloat squareVertices[] = {
        rect.p1.x, rect.p1.y,   // position v0
        rect.p1.x, rect.p2.y,   // position v1
        rect.p2.x, rect.p2.y,   // position v2
        rect.p2.x, rect.p1.y    // position v3
    };
    GLfloat texCoords[] = {
        r.p1.x, r.p1.y,   // Bottom Left Of The Texture
        r.p1.x, r.p2.y,   // Bottom Right Of The Texture
        r.p2.x, r.p2.y,   // Top Left Of The Texture
        r.p2.x, r.p1.y    // Top Right Of The Texture
    };
    glVertexPointer(2, GL_FLOAT, 0, squareVertices);
    glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
void
PainterGLES::drawTexture(const Texture* texture, const Vector2& pos)
{
    Rect2D rect(pos, pos + Vector2(texture->getWidth(), texture->getHeight()));
    drawTextureRect(texture, rect);
}

void
PainterGLES::drawStretchTexture(Texture* texture, const Rect2D& rect)
{
    assert(typeid(*texture) == typeid(TextureGLES));

    if(texture == 0) {
        std::cerr << "Trying to render 0 texture." << std::endl;
#ifdef DEBUG
        assert(false);
#endif
        return;
    }

    drawTextureRect(texture, rect);
}

void
PainterGLES::drawLine( const Vector2 pointA, const Vector2 pointB )
{
    glColor4ub(lineColor.r, lineColor.g, lineColor.b, lineColor.a);
    glDisable(GL_TEXTURE_2D);

    GLfloat lineVertices[] = {
        pointA.x, pointA.y,   // position v0
        pointB.x, pointB.y,   // position v1
    };
    glVertexPointer(2, GL_FLOAT, 0, lineVertices);
    glDrawArrays(GL_LINES, 0, 4);

    glEnable(GL_TEXTURE_2D);
}

void
PainterGLES::fillRectangle(const Rect2D& rect)
{
    glColor4ub(fillColor.r, fillColor.g, fillColor.b, fillColor.a);
    glDisable(GL_TEXTURE_2D);

    GLfloat squareVertices[] = {
        rect.p1.x, rect.p1.y,   // position v0
        rect.p1.x, rect.p2.y,   // position v1
        rect.p2.x, rect.p2.y,   // position v2
        rect.p2.x, rect.p1.y    // position v3
    };
    glVertexPointer(2, GL_FLOAT, 0, squareVertices);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glEnable(GL_TEXTURE_2D);
}

void
PainterGLES::drawRectangle(const Rect2D& rect)
{
    glColor4ub(lineColor.r, lineColor.g, lineColor.b, lineColor.a);
    glDisable(GL_TEXTURE_2D);

    GLfloat lineVertices[] = {
        rect.p1.x, rect.p1.y,   // position v0
        rect.p1.x, rect.p2.y,   // position v1
        rect.p2.x, rect.p2.y,   // position v2
        rect.p2.x, rect.p1.y    // position v3
    };
    glVertexPointer(2, GL_FLOAT, 0, lineVertices);
    glDrawArrays(GL_LINE_LOOP, 0, 4);

    glEnable(GL_TEXTURE_2D);
}

void
PainterGLES::fillPolygon(int numberPoints, const Vector2* points)
{
    glColor4ub(fillColor.r, fillColor.g, fillColor.b, fillColor.a);
    glDisable(GL_TEXTURE_2D);

    GLfloat* polyVertices = new GLfloat[numberPoints*2];
    for( int i = 0; i < numberPoints; i++ ) {
        polyVertices[i*2] = points[i].x;
        polyVertices[i*2 + 1] = points[i].y;
    }
    glVertexPointer(2, GL_FLOAT, 0, polyVertices);
    glDrawArrays(GL_TRIANGLE_FAN, 0, numberPoints);
    delete polyVertices;

    glEnable(GL_TEXTURE_2D);
}

void
PainterGLES::drawPolygon(int numberPoints, const Vector2* points)
{
    glColor4ub(lineColor.r, lineColor.g, lineColor.b, lineColor.a);
    glDisable(GL_TEXTURE_2D);

    GLfloat* lineVertices = new GLfloat[numberPoints*2];
    for( int i = 0; i < numberPoints; i++ ) {
    	lineVertices[i*2] = points[i].x;
    	lineVertices[i*2 + 1] = points[i].y;
    }
    glVertexPointer(2, GL_FLOAT, 0, lineVertices);
    glDrawArrays(GL_LINE_LOOP, 0, numberPoints);
    delete lineVertices;

    glEnable(GL_TEXTURE_2D);
}

void
PainterGLES::setFillColor(Color color)
{
    fillColor = color;
}

void
PainterGLES::setLineColor(Color color)
{
    lineColor = color;
}

void
PainterGLES::translate(const Vector2& vec)
{
    glTranslatef(vec.x, vec.y, 0);
}

void
PainterGLES::pushTransform()
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
}

void
PainterGLES::popTransform()
{
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

void
PainterGLES::setClipRectangle(const Rect2D& rect)
{
    GLfloat matrix[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, matrix);

    int screenHeight = SDL_GetVideoSurface()->h;
    glViewport((GLint) (rect.p1.x + matrix[12]),
               (GLint) (screenHeight - rect.getHeight() - (rect.p1.y + matrix[13])),
               (GLsizei) rect.getWidth(),
               (GLsizei) rect.getHeight());
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(rect.p1.x + matrix[12], rect.p1.x + matrix[12] + rect.getWidth(),
            rect.p1.y + matrix[13] + rect.getHeight(),
            rect.p1.y + matrix[13], -1, 1);
}

void
PainterGLES::clearClipRectangle()
{
    int width = SDL_GetVideoSurface()->w;
    int height = SDL_GetVideoSurface()->h;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(0, width, height, 0, -1, 1);
}

Painter*
PainterGLES::createTexturePainter(Texture* texture)
{
    (void) texture;
    // TODO
    return 0;
}

#endif /* GLES */

